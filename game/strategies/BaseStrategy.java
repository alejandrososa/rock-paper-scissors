package game.strategies;

import game.constants.Figure;

public abstract class BaseStrategy {

    protected Figure getFigure(Integer round) {
        switch (round){
            case 1:
                return Figure.ROCK;
            case 2:
                return Figure.PAPER;
            default:
                return Figure.SCISSORS;
        }
    }

    protected Figure getFigureByName(String name) {
        switch (name.toLowerCase()){
            case "paper":
                return Figure.PAPER;
            case "scissors":
                return Figure.SCISSORS;
            case "rock":
                return Figure.ROCK;
            default:
                return Figure.ROCK;
        }
    }
}
