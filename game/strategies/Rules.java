package game.strategies;

import game.constants.Figure;
import game.contracts.IPlayDecision;
import game.contracts.IRules;
import game.constants.RoundResult;
import game.Decision;

import java.util.HashMap;
import java.util.Map;

public class Rules implements IRules {

    private final Map<Figure, Figure> rules = new HashMap<>();
    {
        rules.put(Figure.SCISSORS, Figure.PAPER);
        rules.put(Figure.PAPER, Figure.ROCK);
        rules.put(Figure.ROCK, Figure.SCISSORS);
    }

    @Override
    public RoundResult getWinner(IPlayDecision decisionPlayerHuman, IPlayDecision decisionPlayerComputer) {
        if (decisionPlayerHuman instanceof Decision && decisionPlayerComputer instanceof Decision) {
            Figure decisionHuman = ((Decision) decisionPlayerHuman).getFigure();
            Figure decisionComputer = ((Decision) decisionPlayerComputer).getFigure();
            return getWinner(decisionHuman, decisionComputer);
        } else {
            throw new IllegalArgumentException("PlayDecision must be IPlayDecision type");
        }
    }

    @Override
    public String getWinnerName(IPlayDecision decisionPlayerHuman, IPlayDecision decisionPlayerComputer) {
        if (decisionPlayerHuman instanceof Decision && decisionPlayerComputer instanceof Decision) {
            Figure decisionHuman = ((Decision) decisionPlayerHuman).getFigure();
            Figure decisionComputer = ((Decision) decisionPlayerComputer).getFigure();
            return getWinnerName(decisionHuman, decisionComputer);
        } else {
            throw new IllegalArgumentException("PlayDecision must be IPlayDecision type");
        }
    }

    private RoundResult getWinner(Figure decisionPlayerA, Figure decisionPlayerB) {
        if (decisionPlayerA == decisionPlayerB) {
            return RoundResult.TIE;
        } else if (rules.get(decisionPlayerA) == decisionPlayerB) {
            return RoundResult.WIN_HUMAN;
        } else {
            return RoundResult.WIN_COMPUTER;
        }
    }

    private String getWinnerName(Figure decisionPlayerA, Figure decisionPlayerB) {
        if (decisionPlayerA == decisionPlayerB) {
            return "(tie)";
        } else if (rules.get(decisionPlayerA) == decisionPlayerB) {
            return "(client wins)";
        } else {
            return "(server wins)";
        }
    }
}
