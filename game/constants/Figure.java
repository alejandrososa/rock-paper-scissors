package game.constants;

public enum Figure {
    ROCK,
    PAPER,
    SCISSORS
}
