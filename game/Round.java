package game;

import game.contracts.IPlayDecision;
import game.contracts.IRound;
import game.contracts.IRules;
import game.constants.RoundResult;

public class Round implements IRound {
    private IRules rules;

    public Round (IRules rules){
        this.rules = rules;
    }

    @Override
    public RoundResult getWinner(IPlayDecision decisionPlayerA, IPlayDecision decisionPlayerB) {
        return rules.getWinner(decisionPlayerA, decisionPlayerB);
    }

    @Override
    public String getWinnerName(IPlayDecision decisionPlayerA, IPlayDecision decisionPlayerB) {
        return rules.getWinnerName(decisionPlayerA, decisionPlayerB);
    }
}
