package game;

import game.constants.RoundResult;
import game.contracts.IGameResult;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class GameResult implements IGameResult {

    private AtomicInteger countWinPlayerHuman = new AtomicInteger(0);
    private AtomicInteger countWinPlayerComputer = new AtomicInteger(0);
    private AtomicInteger countTie = new AtomicInteger(0);
    private Exception exception;
    private List<String> decisionHistory = new ArrayList<>();

    @Override
    public void addRoundResult(RoundResult roundResult) {
        if (roundResult == RoundResult.TIE) {
            countTie.incrementAndGet();
        } else if (roundResult == RoundResult.WIN_HUMAN) {
            countWinPlayerHuman.incrementAndGet();
        } else {
            countWinPlayerComputer.incrementAndGet();
        }
    }

    @Override
    public void addException(Exception exception) {
        this.exception = exception;
    }

    @Override
    public Integer getCountWinPlayerHuman() {
        return countWinPlayerHuman.get();
    }

    @Override
    public Integer getCountWinPlayerComputer() {
        return countWinPlayerComputer.get();
    }

    @Override
    public Integer getCountTie() {
        return countTie.get();
    }

    @Override
    public void saveDecisionHistory(String decisionPlayerA, String decisionPlayerB, String winner) {
        String history = String.format("Chooses, Player figure \"%s\" and Computer \"%s\"... %s /",
                decisionPlayerA, decisionPlayerB, winner);
        decisionHistory.add(history);
    }

    @Override
    public String getResult() {
        String history = "";
        for (int i = 0; i < decisionHistory.size(); i++) {
            history += ("Round Result " + (i + 1) + ": " + decisionHistory.get(i));
        }
        return history;
    }

    @Override
    public void reset() {
        decisionHistory.clear();
        countWinPlayerHuman = new AtomicInteger(0);
        countWinPlayerComputer = new AtomicInteger(0);
        countTie = new AtomicInteger(0);
    }

    @Override
    public Exception getException() {
        return exception;
    }
}
