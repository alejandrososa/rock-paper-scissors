package game.contracts;

import game.contracts.IPlayDecision;

public interface IPlayStrategy {
    IPlayDecision getDecision(Integer round);

    IPlayDecision getDecision(String round);
}
