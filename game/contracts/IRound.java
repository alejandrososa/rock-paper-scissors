package game.contracts;

import game.constants.RoundResult;

public interface IRound {

    RoundResult getWinner(IPlayDecision decisionPlayerA, IPlayDecision decisionPlayerB);

    String getWinnerName(IPlayDecision decisionPlayerA, IPlayDecision decisionPlayerB);
}
