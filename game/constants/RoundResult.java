package game.constants;

public enum RoundResult {
    WIN_HUMAN,
    WIN_COMPUTER,
    TIE
}
