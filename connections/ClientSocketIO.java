package connections;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Scanner;

public class ClientSocketIO implements ISocketOperation {
    private String[] args;
    private int portNumber;
    private InetAddress ipAddress;
    private Socket socket;
    private int rounds = 0;
    private String roundNumber = "a";
    private String playerName;
    private Scanner scanner;

    public ClientSocketIO(String[] arguments) {
        this.args = arguments;
    }

    @Override
    public void execute() throws Exception {
        if (!guardIsCorrectConexion()) {
            return;
        }

        initSettings();

        BYEBYE:
        while (!roundNumber.toLowerCase().equals("q")) {
            scanner = new Scanner(System.in);

            try {
                socket = new Socket(ipAddress, portNumber);

                PrintStream pr = new PrintStream(socket.getOutputStream());

                System.out.println("Enter your name (Press Enter ↵ to quit): ");
                playerName = scanner.nextLine();
                playerName = playerName.equals("") ? "anonymous" : playerName;

                System.out.println("Enter the number of rounds (Press Q to quit): ");
                roundNumber = scanner.nextLine();

                if (roundNumber.toLowerCase().equals("q")) {
                    break BYEBYE;
                } else {
                    rounds = Integer.parseInt(roundNumber);
                    if (rounds <= 0) {
                        System.out.println("You cannot enter 0 or lower.");
                    }
                }

                String[] userEntries = new String[rounds];
                for (int i = 0; i < rounds; i++) {
                    System.out.print("RoundResult " + (i + 1) + ": ");
                    userEntries[i] = scanner.nextLine();
                }

                String msgToServer = roundNumber;
                for (int i = 0; i < rounds; i++) {
                    msgToServer += '/' + userEntries[i];
                }

                msgToServer = playerName + '/' + msgToServer;

                InputStream stream = new ByteArrayInputStream(msgToServer.getBytes("UTF-8"));
                InputStreamReader rd = new InputStreamReader(stream);
                BufferedReader ed = new BufferedReader(rd);

                String temp = ed.readLine();

                pr.println(temp);

                // Getting the result from server
                BufferedReader gt = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String tm = gt.readLine();

                String[] tokens = tm.split("/");
                for (String t : tokens) {
                    System.out.println(t);
                }

                System.out.println("\n");
                close();
            } catch (IOException e) {
                throw new Exception("Server is not available");
            }
        }
    }

    private void initSettings() {
        try {
            ipAddress = InetAddress.getByName(args[0]);
            portNumber = Integer.parseInt(args[1]);
        } catch (UnknownHostException | NumberFormatException e) {
            System.out.println("Error trying to connect to the client.");
            if (e instanceof UnknownHostException) {
                System.out.println("Wrong host. " + e.getMessage());
            }
            if (e instanceof NumberFormatException) {
                System.out.println("Wrong port. " + e.getMessage());
            }
        }
    }

    @Override
    public void close() {
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                System.out.println("Error closing client socket. " + e.getMessage());
            }
        }
    }

    private Boolean guardIsCorrectConexion() {
        Boolean checkIsValid = false;

        if (args.length == 2) {
            checkIsValid = true;
        }

        return checkIsValid;
    }
}
