import connections.ClientSocketIO;
import connections.ServerSocketIO;
import connections.SocketManager;
import game.Game;
import game.GameResult;
import game.Round;
import game.strategies.Rules;

public class RockPaperScissors {
    public static void main(String[] args) {

        guarValidArguments(args);

        try {
            Round round = new Round(new Rules());
            GameResult gameResult = new GameResult();
            Game game = new Game(round, gameResult);

            //Listen requests
            SocketManager socketManager = new SocketManager();
            socketManager.execute(new ServerSocketIO(args, game));
            socketManager.execute(new ClientSocketIO(args));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private static void guarValidArguments(String[] args) {
        if (args.length == 0) {
            System.out.println("Invalid arguments!");
        }
    }
}