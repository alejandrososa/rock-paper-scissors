package game.strategies;

import game.contracts.IPlayDecision;
import game.Decision;
import game.contracts.IPlayStrategy;

import java.util.Random;

public class ComputerStrategy extends BaseStrategy implements IPlayStrategy {
    private final Random rdm = new Random();

    @Override
    public IPlayDecision getDecision(Integer round) {
        return Decision.create(getFigure(getRandomValueFromOneToThree()));
    }

    @Override
    public IPlayDecision getDecision(String round) {
        return Decision.create(getFigureByName(round));
    }

    private int getRandomValueFromOneToThree() {
        return rdm.nextInt(3) + 1;
    }
}
