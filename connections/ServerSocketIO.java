package connections;

import game.GameResult;
import game.contracts.IGame;
import game.strategies.ComputerStrategy;
import game.strategies.HumanStrategy;
import player.ComputerPlayer;
import player.GuestPlayer;
import player.IPlayer;
import player.RegisteredPlayer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;
import java.util.Random;

public class ServerSocketIO implements ISocketOperation {

    private String[] args;
    private IGame game;
    private GameResult result;
    private Socket socket;
    private ServerSocket serverSocket;
    private int portNumber;
    private BufferedReader lector;
    private int rounds = 0;
    private String playerName;
    private IPlayer playerA, playerB;
    private String delims = "/";

    public ServerSocketIO(String[] arguments, IGame game) {
        this.args = arguments;
        this.game = game;
    }

    @Override
    public void execute() throws Exception {
        if (!guardIsCorrectConexion()) {
            return;
        }

        System.out.println("Waiting for client...");


        while (true) {

            try {

                initSettings();

                if (socket != null) {
                    System.out.println("Client connected");
                }

                lector = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                String tmp = lector.readLine();
                String[] tokens = tmp.split(delims);
                playerName = tokens[0];
                rounds = Integer.parseInt(tokens[1]);

                initGamePlayers();

                for (int i = 2; i < (rounds + 2); i++) {
                    game.setDecisionPlayerA(tokens[i]);
                    System.out.println("Client has sent " + tokens[i] + " shapes...");
                }

                System.out.println("Shapes are chosen...");

                PrintStream pr = new PrintStream(socket.getOutputStream());

                result = game.getGameResult(rounds);

                if (result.getException() == null) {
                    sendMessageToClient(rounds, pr);

                } else {
                    System.out.println("Error: " + result.getException().getMessage());
                    result.getException().printStackTrace();
                }

                close();

            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                break;
            }
        }

        System.out.println("Client disconnected");
        System.out.println("Server terminating...");
    }

    private void sendMessageToClient(int rounds, PrintStream pr) {
        String msgToClient = "" + delims;
        String greeting = playerA.getGreeting();
        if(greeting != null && !greeting.isEmpty()) {
            msgToClient += (greeting + delims);
        }

        String[] histories = result.getResult().split(delims);
        for (String t : histories) {
            msgToClient += (t) + delims;
        }

        msgToClient += ("Player Human wins " + result.getCountWinPlayerHuman() + " of " + rounds + " games. " + delims);
        msgToClient += ("Player Computer wins " + result.getCountWinPlayerComputer() + " of " + rounds + " games. " + delims);
        msgToClient += ("Tie: " + result.getCountTie() + " of " + rounds + " games. " + delims);
        pr.println(msgToClient);

        String[] result = msgToClient.split(delims);
        for (String t : result) {
            msgToClient += (t) + ". " + delims;
        }

        game.reset();

        System.out.println("\n");
    }

    private void initGamePlayers() {
        playerA = new GuestPlayer(playerName);

        if(!playerName.equals("anonymous")){
            playerA = new RegisteredPlayer(playerName);
        }

        playerA.setStrategy(new HumanStrategy());

        playerB = new ComputerPlayer();
        playerB.setStrategy(new ComputerStrategy());

        game.setPlayerA(playerA);
        game.setPlayerB(playerB);
    }

    private void initSettings() throws IOException {
        portNumber = Integer.parseInt(args[0]);
        serverSocket = new ServerSocket(portNumber);
        socket = serverSocket.accept();
    }

    @Override
    public void close() {
        if (serverSocket != null) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                System.out.println("Error closing server socket. " + e.getMessage());
            }
        }
    }

    private Boolean guardIsCorrectConexion() {
        Boolean checkIsValid = false;

        if (args.length == 1) {
            checkIsValid = true;
        }

        return checkIsValid;
    }
}
