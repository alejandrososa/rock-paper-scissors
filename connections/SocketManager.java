package connections;

import java.util.ArrayList;
import java.util.List;

public class SocketManager {
    private final List<ISocketOperation> socketOperations = new ArrayList<>();

    public void execute(ISocketOperation socketOperation) throws Exception {
        socketOperations.add(socketOperation);
        socketOperation.execute();
    }
}
