package game.contracts;

import game.constants.Figure;

public interface IPlayDecision {
    Figure getFigure();
}
