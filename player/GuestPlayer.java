package player;

import game.contracts.IPlayDecision;
import game.contracts.IPlayStrategy;

public class GuestPlayer implements IPlayer {

    private String name;
    private IPlayStrategy strategy;

    public GuestPlayer(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public IPlayDecision getDecision(Integer round) {
        return strategy.getDecision(round);
    }

    @Override
    public IPlayDecision getDecision(String input) {
        return strategy.getDecision(input);
    }

    @Override
    public String getGreeting() {
        return "";
    }

    @Override
    public void setStrategy(IPlayStrategy strategy) {
        this.strategy = strategy;
    }
}
