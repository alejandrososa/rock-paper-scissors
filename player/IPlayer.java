package player;

import game.contracts.IPlayDecision;
import game.contracts.IPlayStrategy;

public interface IPlayer {

    String getName();

    IPlayDecision getDecision(Integer round);

    IPlayDecision getDecision(String input);

    String getGreeting();

    void setStrategy(IPlayStrategy strategy);
}
