package game;

import game.contracts.IGame;
import game.contracts.IPlayDecision;
import player.IPlayer;

import java.util.ArrayList;

public class Game implements IGame {

    private GameResult gameResult;
    private Round round;
    private IPlayer playerA, playerB;
    private ArrayList<String> decisionsPlayerA, decisionsPlayerB;

    public Game(Round round, GameResult gameResult) {
        this.round = round;
        this.gameResult = gameResult;

        this.decisionsPlayerA = new ArrayList<String>();
        this.decisionsPlayerB = new ArrayList<String>();
    }

    @Override
    public void setPlayerA(IPlayer player) {
        this.playerA = player;
    }

    @Override
    public void setPlayerB(IPlayer player) {
        this.playerB = player;
    }

    @Override
    public void setDecisionPlayerA(String input) {
        this.decisionsPlayerA.add(input);
    }

    @Override
    public void setDecisionPlayerB(String input) {
        this.decisionsPlayerB.add(input);
    }

    @Override
    public GameResult getGameResult(Integer roundCount) {
        roundCount = roundCount == 0 ? 0 : roundCount - 1;
        try {
            for (int i = 0; i <= roundCount; i++) {
                IPlayDecision decisionPlayerA = playerA.getDecision(decisionsPlayerA.get(i));
                IPlayDecision decisionPlayerB = playerB.getDecision(i);

                gameResult.addRoundResult(round.getWinner(decisionPlayerA, decisionPlayerB));

                String winner = round.getWinnerName(decisionPlayerA, decisionPlayerB);
                gameResult.saveDecisionHistory(
                        decisionPlayerA.getFigure().toString(),
                        decisionPlayerB.getFigure().toString(),
                        winner
                );
            }

            return gameResult;
        } catch (Exception exp) {
            gameResult.addException(exp);
            return gameResult;
        }
    }

    @Override
    public void reset() {
        decisionsPlayerA.clear();
        decisionsPlayerB.clear();
        gameResult.reset();
    }
}
