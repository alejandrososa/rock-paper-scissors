package game.contracts;

import game.constants.RoundResult;

public interface IGameResult {
    void addRoundResult(RoundResult roundResult);

    void addException(Exception exception);

    Integer getCountWinPlayerHuman();

    Integer getCountWinPlayerComputer();

    Integer getCountTie();

    void saveDecisionHistory(String decisionPlayerA, String decisionPlayerB, String winner);

    String getResult();

    void reset();

    Exception getException();
}