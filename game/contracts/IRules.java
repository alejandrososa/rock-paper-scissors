package game.contracts;


import game.constants.RoundResult;

public interface IRules {
    RoundResult getWinner(IPlayDecision decisionPlayerA, IPlayDecision decisionPlayerB);

    String getWinnerName(IPlayDecision decisionPlayerHuman, IPlayDecision decisionPlayerComputer);
}
