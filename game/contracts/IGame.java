package game.contracts;

import game.GameResult;
import player.IPlayer;

public interface IGame {
    void setPlayerA(IPlayer player);

    void setPlayerB(IPlayer player);

    void setDecisionPlayerA(String input);

    void setDecisionPlayerB(String input);

    GameResult getGameResult(Integer roundCount);

    void reset();
}
