package game.strategies;

import game.contracts.IPlayDecision;
import game.Decision;
import game.contracts.IPlayStrategy;

public class HumanStrategy extends BaseStrategy implements IPlayStrategy {

    @Override
    public IPlayDecision getDecision(Integer round) {
        return Decision.create(getFigure(round));
    }

    @Override
    public IPlayDecision getDecision(String round) {
        return Decision.create(getFigureByName(round));
    }
}
