package connections;

public interface ISocketOperation {
    void execute() throws Exception;

    void close();
}
