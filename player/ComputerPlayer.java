package player;

import game.contracts.IPlayDecision;
import game.contracts.IPlayStrategy;

public class ComputerPlayer implements IPlayer{

    private String name;
    private IPlayStrategy strategy;

    public ComputerPlayer()
    {
        this.name = "Computer";
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public IPlayDecision getDecision(Integer round) {
        return strategy.getDecision(round);
    }

    @Override
    public IPlayDecision getDecision(String input) {
        return strategy.getDecision(input);
    }

    @Override
    public String getGreeting() {
        return "Hello, I am the automatic player";
    }

    @Override
    public void setStrategy(IPlayStrategy strategy) {
        this.strategy = strategy;
    }
}
