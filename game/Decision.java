package game;

import game.constants.Figure;
import game.contracts.IPlayDecision;

public class Decision implements IPlayDecision {
    private final Figure figure;

    private Decision(Figure figure) {
        this.figure = figure;
    }

    public static Decision create(Figure figure) {
        return new Decision(figure);
    }

    public Figure getFigure() {
        return this.figure;
    }
}

